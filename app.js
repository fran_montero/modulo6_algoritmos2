/* ENIGMA */

const enigOriginal = document.getElementById('enigOriginal');
const enigEncriptado = document.getElementById('enigEncriptado');

var plainAlphabet = "abcdefghijklmnopqrstuvwxyz:()!¡,' ";
var encryptedAlphabet = "qw,ert(yuio'pa:sdfg!hjklz¡xcv)bnm ";


function encriptar() {
    let text = enigOriginal.value;
    let resultado = getTexto(text,plainAlphabet, encryptedAlphabet);
    enigEncriptado.innerHTML = resultado;    
}

function desencriptar() {
    let text = enigEncriptado.value;
    let resultado = getTexto(text,encryptedAlphabet, plainAlphabet);
    enigOriginal.innerHTML = resultado;    
}

function getTexto(cadenaRecibida, cadenaEquivalente, cadenaAcomparar) {
    let resultado = "";
    for (let i = 0; i < cadenaRecibida.length; i++) {
        let posicion = cadenaEquivalente.indexOf(cadenaRecibida[i]);
        let equivalencia = cadenaAcomparar.charAt(posicion);
        resultado +=equivalencia;        
    }
    return resultado;
}



/* NÚMERO ALEATORIO */

var randomPick = (n, min, max) => {
    let result = [];
    let init = 0;
    if (n <= (max-min)) {
        while (init < n) {
            let num = Math.floor(Math.random() * (max - min +1) + min);
            if(!result.includes(num)) {
                result.push(num);
                init++;
            }
        }
        console.log(result);
    } else {
        console.log("El margen entre máximo y mínimo debe ser mayor que la cantidad de números pedidos");
    }   
  }

randomPick(10,1,100);



